HexTile_HeightUnit=16;
HexTile_BaseHeight=10;					//hauteurHex=10;
HexTile_BaseTopThickness=2;				//epaisseurTopFond = 2;
HexTile_BaseShellThibaseTopckness=2;	//epaisseurShell = 2;
HexTile_Diameter=56;					//diametreHex=56;

rI = (HexTile_Diameter/2)*(sqrt(3)/2);
rC = (HexTile_Diameter/2);

use <./tiles/tile_hex_style1.scad>;
use <./tiles/tile_hex_downsteps.scad>;
use <./tiles/tile_hex_lego.scad>

module TILE_hex(height=HexTile_HeightUnit*1, style="default") {

	if(style=="style1") {
		TILE_hex_style1(height=height);
	}	
	else if(style=="downsteps") {
		TILE_hex_downsteps(height=height);
	}	
	else if(style=="lego") {
		TILE_hex_lego(height=height);
	}	
	else if(style=="default") {
		TILE_hex_default(height=height);
	}	
	else {
		echo("Unknown style: ", style);
	}
	
}

// hex tile shape default
module TILE_hex_default(height=HexTile_HeightUnit*1) {

	//Reste pattern = hauteurPattern - hauteurHex !
	baseTop = height-HexTile_BaseHeight;

	largeurPicot = 4;
	epaisseurPicot = HexTile_BaseShellThibaseTopckness;

	largeurRenfort = largeurPicot*1.75;
	epaisseurRenfort = epaisseurPicot;

	rotate(30,[0,0,1])
	{
		difference() {

			//Matiere "ajoutée"

			union() {

				//Shell
				difference() {
					cylinder(HexTile_BaseHeight, rC, rC, $fn=6);
					cylinder(HexTile_BaseHeight, rC-(HexTile_BaseShellThibaseTopckness/(sqrt(3)/2)), rC-HexTile_BaseShellThibaseTopckness, $fn=6);
				}

				//Top fond
				translate([0,0,HexTile_BaseHeight-HexTile_BaseTopThickness])
					cylinder(HexTile_BaseTopThickness, rC-HexTile_BaseShellThibaseTopckness, rC-HexTile_BaseShellThibaseTopckness, $fn=6);

				//Hexa qui commence depuis le top de la base, jusqu'a la hauteur souhaité
				translate([0,0,HexTile_BaseHeight])
					cylinder(baseTop, rC, rC, $fn=6);				

				//Picots+renforts pour chaque coté
				for (a = [0:5])
				{
					rotate([0,0,a*60])
					{
						//Picot
						largeurBase = largeurPicot;
						largeurHaut = largeurBase/2;

						picotX = largeurBase/2 + (rC/4); 	//picot centré au 3/4 du coté exterieur
						picotY = rI+epaisseurPicot-.2; 		//base du picot posée sur le coté exterieur

						translate([picotX,picotY,0])
						{
							rotate(180, [0,0,1])
							{
								linear_extrude(HexTile_BaseHeight)
								{
									polygon([
										[0,0],
										[largeurBase,0],
										[largeurBase - (largeurHaut/3),epaisseurPicot],
										[largeurHaut/3,epaisseurPicot]
									]);
								}
							}
						}

						//Renfort arriere picot inverse
						renfortX=-largeurRenfort/2 - (rC/4);
						renfortY=rI-HexTile_BaseShellThibaseTopckness-epaisseurRenfort+.1;

						translate([renfortX, renfortY, 0])
							cube([largeurRenfort, epaisseurRenfort, HexTile_BaseHeight-HexTile_BaseTopThickness]);
					}
				}

				//Si un enfant est fourni, enleve tout sur l'enfant qui depasse du tile
				if($children>0) {
					rayonExterne = HexTile_Diameter;
					rayonInterne = HexTile_Diameter/2-(HexTile_BaseShellThibaseTopckness*0.75);
					difference() {
						children(0);
						difference() {
							cylinder(height*2, rayonExterne, rayonExterne, $fn=6);
							cylinder(height*2, rayonInterne, rayonInterne, $fn=6);
						}
					}
				}
			}

			//Matiere "enlevée"
			union() {

				//Raynures
				for (a = [0:5])
				{
					rotate([0,0,a*60])
					{
						//Picot
						largeurBase = largeurPicot;
						largeurHaut = largeurBase/2;

						rainureX = -largeurBase/2 - (rC/4); 	//picot centré au 3/4 du coté exterieur
						rainureY = rI-epaisseurPicot+.1; 			//base du picot posée sur le coté exterieur

						translate([rainureX,rainureY,0])
						{
							linear_extrude(height)
							{
								polygon([
									[0,0],
									[largeurBase,0],
									[largeurBase - (largeurHaut/3),epaisseurPicot],
									[largeurHaut/3,epaisseurPicot]
								]);
							}
						}
					}
				}

				//Si un deuxieme enfant est fourni, enleve la matiere de l'enfant sur le tile
				if($children>1) {
					children(1);
				}
			}
		}
	}
}

// hex object cutting shape (contour)
module CUT_hex(height) {
	rotate(30,[0,0,1])
		cylinder(height, (HexTile_Diameter/2)+.1, (HexTile_Diameter/2)+.1, $fn=6);
}


//---------------------------------------------------------

//Transform from offset to axial hexagonal coordonates system
function MAP_hex_pointOffset2Axial(point) =
	let (pointX=point[0])
	let (pointY=point[1])
    let (newX=(pointX-(floor(pointY/2))))
	[pointY,newX];

//Place all provided children on the offseted hex map
module MAP_hex(pattern, spacing=0) {
	for(i=[0:len(pattern)-1]) {
		moduleCoords = pattern[i];
		PLACE_hex(moduleCoords, spacing=spacing) {
			children(i);
		}
	}
}

//Place an objet on the offset hex map
module PLACE_hex(coords, spacing=0) {

	axialCoords = MAP_hex_pointOffset2Axial(coords);

    tileX = axialCoords[0];
    tileY = axialCoords[1];

    tileSpacing = spacing > 0 ? spacing : 0;

    outerRadius = (HexTile_Diameter + tileSpacing) / 2;
    innerRadius = outerRadius * (sqrt(3)/2);

    offsetedZ = (tileX - abs(tileX % 2)) / 2;
    positionOffest = tileX % 2 != 0 ? innerRadius : 0;

    worldX = tileX * outerRadius * 1.5;
    worldY = (tileY + offsetedZ) * innerRadius * 2 + positionOffest;

    translate([worldY,worldX,0])
        children();
}

//Place object on hex grid, and keep the [........]
module PLACE_hex_cut(coords, spacing=0) {
	axialCoords = MAP_hex_pointOffset2Axial(coords);
	objectX=axialCoords[0]*spacing;
	objectY=axialCoords[1]*spacing;
	translate([objectX,objectY])
		children();
}

//Make a pattern of children (if provided), or with integrated hex tiles
module PATTERN_hex(pattern=[], height=HexTile_HeightUnit, style="default", spacing=false) {
	for(i=[0:len(pattern)-1]) {
		tile=pattern[i];
		PLACE_hex(coords=tile, spacing=spacing) {
			//if children provided, use children, else use integrated hex tile(s)
			if($children==0) {
				TILE_hex(height=height, style=style);
			} else {
				children();
			}
		}
	}
}

module PATTERN_hex_cut(pattern=[], height=HexTile_HeightUnit, spacing=false) {
	for(i=[0:len(pattern)-1]) {
		tile=pattern[i];
		render() {
			intersection() {
				PLACE_hex(coords=tile, spacing=spacing) {
					CUT_hex(height);
				}
				PLACE_hex_cut(coords=tile, spacing=spacing) {
					children();
				}
			}
		}
	}
}
