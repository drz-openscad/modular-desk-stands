include <./LIB_STAND.scad>

// Include sub-modules
use <./modules/module_xpider.scad>
use <./modules/module_lego_row3.scad>
use <./modules/module_vape_box1.scad>
use <./modules/module_8bitdo_gamepads.scad>
use <./modules/module_hubsan_batteries.scad>
use <./modules/module_usb.scad>
use <./modules/module_sd.scad>

// Modules map
map = [
		[0,1],
	[0,0],
];


MAP_hex(map) {

	MODULE_8bitdo(moduleHeight=HexTile_HeightUnit*2);		//2U height
	MODULE_8bitdo(moduleHeight=HexTile_HeightUnit*1.25);	//1.25U height

	//TODO: placer ceux la:
	MODULE_xpider();
	MODULE_lego_row3();
	MODULE_vape_box1();
	TILE_usb();
	TILE_usb();
	TILE_sd();
	MODULE_hubsan_batteries();
}
