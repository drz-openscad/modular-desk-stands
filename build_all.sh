#!/bin/bash

echo "Demos"
openscad -o stl/demo_patterns_cubes.stl demos/demo_patterns_cubes.scad
openscad -o stl/demo_patterns_hex.stl demos/demo_patterns_hex.scad

echo "Tests"
openscad -o stl/test_forme_hex.stl tests/test_forme_hex.scad
openscad -o stl/test_pattern_hex.stl tests/test_pattern_hex.scad
openscad -o stl/test_map_hex.stl tests/test_map_hex.scad
openscad -o stl/test_module.stl tests/test_module.scad

echo "Led diffuser"
openscad -o stl/diffuser_hex.stl modules/diffuser_hex.scad

echo "Modules"
openscad -o stl/module_8bitdo_gamepads.stl modules/module_8bitdo_gamepads.scad
openscad -o stl/module_gameboy.stl modules/module_gameboy.scad
openscad -o stl/module_hubsan_batteries.stl modules/module_hubsan_batteries.scad
openscad -o stl/module_lego_row3.stl modules/module_lego_row3.scad
openscad -o stl/module_sd.stl modules/module_sd.scad
openscad -o stl/module_usb.stl modules/module_usb.scad
openscad -o stl/module_vape_box1.stl modules/module_vape_box1.scad
openscad -o stl/module_xpider.stl modules/module_xpider.scad

echo "Tiles"
openscad -o stl/tile_hex_downsteps.stl tiles/tile_hex_downsteps.scad
openscad -o stl/tile_hex_lego.stl tiles/tile_hex_lego.scad
openscad -o stl/tile_hex_style1.stl tiles/tile_hex_style1.scad

echo "ALL IN ONE PREVIEW"
openscad -o stl/my_desktop_stands.stl my_desktop_stands.scad


echo "Done !"
