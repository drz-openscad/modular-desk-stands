include <../LIB_STAND.scad>

module MODULE_lego_row3(moduleHeight=HexTile_HeightUnit*2, spacing=false) {

	row3Pattern = [
		[-1,0],		[0,0],		[1,0]
	];

	PATTERN_hex(pattern=row3Pattern, spacing=spacing, height=moduleHeight, style="lego");
}

MODULE_lego_row3();