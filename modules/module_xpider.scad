include <../LIB_STAND.scad>

module robot(moduleHeight) {
	translate([0,0,45+moduleHeight])
		rotate(-30+180,[0,0,1])
		rotate(-90,[0,1,0])
			import("../stl/not-mine/1117_xpider_v1.stl");

}

module MODULE_xpider(moduleHeight=HexTile_HeightUnit*1, spacing=false) {

	patternLegs = [
			[-1,1],			[0,1],

		[-1,0],		/*[0,0]*/,		[1,0],

			[-1,-1], 		[0,-1]
	];

	PATTERN_hex(pattern=patternLegs, height=moduleHeight, spacing=spacing, style="downsteps");
	
	patternCenter = [
		[0,0]
	];

	PATTERN_hex(pattern=patternCenter, height=moduleHeight, spacing=spacing, style="style1") ;

	//For the fun, render the robot !
	%robot(moduleHeight);
}

MODULE_xpider();
