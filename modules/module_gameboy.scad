include <../LIB_STAND.scad>

module gameboy() {
	
	$fn=100;
	x=70;
	radius=20;
	slotWidth=32;
	angle=10;
	rotate(-angle,[1,0,0]) {
		
		translate([0,0,76])
			scale([.38,.38,.38])
				group() {
					import("../stl/not-mine/Gameboy_low2.stl", center=false);
				}


		/*
		rotate(90,[1,0,0]) {
			translate([0,0,-slotWidth/2]) {
				linear_extrude(height=slotWidth) {
					hull() {
						translate([(-x/2)+(radius/2), radius, 0])
							circle(r=radius);
						translate([(x/2)-(radius/2), radius, 0])
							circle(r=radius);
						translate([(x/2)-(radius/2), radius+125, 0])
							circle(r=radius);
						translate([(-x/2)+(radius/2), radius+125, 0])
							circle(r=radius);
					}
				}
			}
		}*/
	}
	//translate([0,0,100])
	//scale([.5,.5,.5])
		//import("../stl/not-mine/Gameboy.stl", center=false);
}

module MODULE_gameboy(moduleHeight=HexTile_HeightUnit*1.5, spacing=false) {

	cutHeight = 12;
	
	pattern = [
		[-1,1],[0,1],
	 [-1,0],[0,0],[1,0],
	];
	
	
	//pattern = [[-1,1]];
	//pattern = [[0,1]];
	//pattern = [[-1,0]];
	//pattern = [[0,0]];
	//pattern = [[1,0]];
	
	
	UTIL_makeStand(moduleHeight, cutHeight) {

		//BASE = pattern socle
		PATTERN_hex(pattern=pattern, spacing=spacing, height=moduleHeight, style="style1");

		//CUT = objet decoupé avec pattern socle
		PATTERN_hex_cut(pattern=pattern, spacing=spacing, height=cutHeight) {

			translate([0,0])
				PLACE_hex([0, 0])
					#gameboy();
		}
	}
}


MODULE_gameboy(spacing=0);