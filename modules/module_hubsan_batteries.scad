include <../LIB_STAND.scad>

battery_number = 3;
tray_depth = 0;
gap = 3;
battery_width = 22;
battery_height = 16;
battery_depth = 9;
longueur_slots = battery_number*battery_depth + ((battery_number-1)*gap);
largeur_slots = battery_width + (1.5*gap);

module logo_charge() {
	translate([-10,6,8])
		rotate(0,[0,0,1])
			linear_extrude(height = 6, convexity = 0)
				scale([2,2,1])
					import (file = "../2d/dxf/Battery_charge.dxf");
}

module logo_empty() {
	translate([rI/3,rI/3,8])
		rotate(90+180,[0,0,1])
			linear_extrude(height = 6, convexity = 0)
				scale([2,2,1])
					import (file = "../2d/dxf/Battery_empty.dxf");
}

module logo_full() {
	
	translate([-rI-2,rI/3,8])
		rotate(90+180,[0,0,1])
			linear_extrude(height = 6, center = false, convexity = 0)
				scale([2,2,1])
					import (file = "../2d/dxf/Battery_full.dxf" );
}

module charger() {
	y= -(rI/2)-(gap/2); 
	x= -battery_width/2 - 4;
	rotate(90,[0,0,1]) {
		for (i = [0 : 1]) {			
			translate([x, y + (i+1)*gap + i*battery_depth, -2])
				cube([battery_width, battery_depth,battery_height+30]);
		}
	}
}

module slots() {
	h= -longueur_slots /2; 
	w= (+gap) - largeur_slots;
	rotate(0,[0,0,1]) {
		for (i = [0 : battery_number-1])
		{			
			translate([-battery_width/2, h+   (i*(gap + battery_depth)),i*3])
				cube([battery_width, battery_depth,battery_height+30]);
			//translate([w , h+   (i*(gap + battery_depth)),i*3])
			//	cube([battery_width, battery_depth,battery_height+30]);
		}
	}
}

module MODULE_hubsan_batteries(moduleHeight=HexTile_HeightUnit*1.5, spacing=false)
{
	
	cutHeight=12;

	patternTri3 = [
		[0,0],[1,0],
		   [0,-1]
	];

	UTIL_makeStand(moduleHeight, cutHeight) {

		//BASE = pattern socle
		PATTERN_hex(pattern=patternTri3, spacing=spacing, height=moduleHeight, style="downsteps");

		//CUT = objet decoupé avec pattern socle
		PATTERN_hex_cut(pattern=patternTri3, spacing=spacing, height=cutHeight) {
			PLACE_hex([0,0]) {
				#logo_empty();
				#slots();
			}
			PLACE_hex([1,0]) {
				#logo_full();
				#slots();
			}
			PLACE_hex([0,-1]) {
				#logo_charge();
				#charger();
			}
		}
	}
}

MODULE_hubsan_batteries();