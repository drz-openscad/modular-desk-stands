include <../LIB_STAND.scad>

module gamepad() {
	$fn=100;
	x=52;
	radius=20;
	slotWidth=14;
	angle=15;
	rotate(-angle,[1,0,0]) {
		rotate(90,[1,0,0]) {
			translate([0,0,-slotWidth/2]) {
				linear_extrude(height=slotWidth) {
					hull() {
						translate([(-x/2)+(radius/2), radius, 0])
							circle(r=radius);
						translate([(x/2)-(radius/2), radius, 0])
							circle(r=radius);
					}
				}
			}
		}
	}
}

module MODULE_8bitdo(moduleHeight=HexTile_HeightUnit*1.25, spacing=false) {

	cutHeight = 5;
	
	row2Pattern = [
		[0,0],[1,0]
	];

	UTIL_makeStand(moduleHeight, cutHeight) {

		//BASE = pattern socle
		PATTERN_hex(pattern=row2Pattern, spacing=spacing, height=moduleHeight, style="downsteps");

		//CUT = objet decoupé avec pattern socle
		PATTERN_hex_cut(pattern=row2Pattern, spacing=spacing, height=cutHeight) {

			translate([0,-2])
				PLACE_hex([.5, 0])
					#gamepad();
		}
	}
}

MODULE_8bitdo();