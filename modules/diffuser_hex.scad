include <../LIB_STAND.scad>

baseHeight = 4;
insideHeight = 4;

difference() {
	translate([0,0,-baseHeight])
		cylinder(insideHeight+baseHeight-.2, (HexTile_Diameter/2)-.2, (HexTile_Diameter/2)-.2 , $fn=6);
	
	rotate(30, [0,0,1])
		TILE_hex(HexTile_BaseHeight);
	
	difference() {
		translate([0,0,0])
			cylinder(HexTile_BaseHeight, HexTile_Diameter/2, HexTile_Diameter/2 , $fn=6);
		translate([0,0,0])
			cylinder(HexTile_BaseHeight, (HexTile_Diameter/2)-HexTile_BaseShellThibaseTopckness-.2, (HexTile_Diameter/2)-HexTile_BaseShellThibaseTopckness-.2 , $fn=6);
	}
	
	translate([0,0,0])
		cylinder(HexTile_BaseHeight, (HexTile_Diameter/2.75), (HexTile_Diameter/2.75),  $fn=6);
	
	translate([0,0,insideHeight])
		sphere(insideHeight*2, insideHeight, $fn=6);

}

rotate(30, [0,0,1])
	%TILE_hex(HexTile_BaseHeight);