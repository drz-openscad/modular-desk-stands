include <../LIB_STAND.scad>

//*Usb holder from:
//	Filename:	UsbStickSdCardHolderV2.scad
//	Author:		Robert H. Morrison
//	Date:		Saturday, 02 Feb 2013


module TILE_usb(tileHeight=HexTile_HeightUnit*1) {
	
	USB_sticks = 4;
	USB_gap = 4;
	USB_width = 5;
	USB_length = 13;
	USB_depth = 13;
	USB_Holder_Height = tileHeight+HexTile_BaseHeight;
	USB_Holder_Width = 32;
	USB_Holder_Length = USB_sticks*(USB_width + USB_gap)+6-USB_gap;

	TILE_hex_default(height=tileHeight) {

		//Ajout matiere
		cube([0,0,0]);		//TODO: fix: forme vide

		//Enlevement matiere
		union() {
			rotate(150,[0,0,1])
			for (i=[1:USB_sticks])
				translate([5-USB_Holder_Length/2+(i-1)*(USB_width+USB_gap),0,USB_Holder_Height-USB_depth/2])
					#cube([USB_width,USB_length,USB_depth+1], true);
		}
	}
}

TILE_usb();