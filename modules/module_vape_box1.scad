include <../LIB_STAND.scad>
include <../LIB_VAPE.scad>

use <../tiles/tile_hex_style1.scad>

module cut() {

		PLACE_hex([-1,1]) {
			support_ato22mm();
		}
		
		PLACE_hex([-.25,0.5]) {
			support_sigelei();
		}

		PLACE_hex([1,0]) {
			support_bouteille30ml();
		}

/*
		PLACE_hex([0.5,1.25]) {
			support_driptip();
		}

		PLACE_hex([0.5,0.75]) {
			support_driptip();
		}

		PLACE_hex([-.5,-.25]) {
			support_driptip();
		}

		PLACE_hex([-.5,.25]) {
			support_driptip();
		}
	*/
}

module MODULE_vape_box1(moduleHeight=HexTile_HeightUnit*1.5, spacing=false)
{
	cutHeight=12;
	
	pattern = [
		[-1,1], 		[0,1],
				[0,0],			[1,0]
	];

	UTIL_makeStand(moduleHeight, cutHeight) {

		//BASE = pattern socle
		PATTERN_hex(pattern=pattern, spacing=spacing, height=moduleHeight, style="downsteps");

		//CUT = objet decoupé avec pattern socle
		PATTERN_hex_cut(pattern=pattern, spacing=spacing, height=cutHeight) {
			#cut();
		}
	}
}

MODULE_vape_box1();

