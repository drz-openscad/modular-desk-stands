include <../LIB_STAND.scad>

//*Micro sd & Usb holder from:
//	Filename:	UsbStickSdCardHolderV2.scad
//	Author:		Robert H. Morrison
//	Date:		Saturday, 02 Feb 2013

module MODULE_sd(moduleHeight=HexTile_HeightUnit*1) {
	
	USB_sticks = 4;
	USB_gap = 4;
	USB_width = 5;
	USB_length = 13;
	USB_depth = 13;
	USB_Holder_Height = moduleHeight+HexTile_BaseHeight;
	USB_Holder_Width = 32;
	USB_Holder_Length = USB_sticks*(USB_width + USB_gap)+6-USB_gap;

	MicroSD_gap = 3.4; 
	MicroSD_width = 1;
	MicroSD_length = 9;
	MicroSD_depth = 15;
	MicroSD_cards = (USB_Holder_Length-3)/(MicroSD_width+MicroSD_gap);

	SD_cards = 4;
	SD_gap = 2; 
	SD_width = 2.64;
	SD_length = 26.15;
	SD_depth = 15;
	SD_Holder_Height = moduleHeight+HexTile_BaseHeight;
	SD_Holder_Width = 32;
	SD_Holder_Length = SD_gap+SD_cards*(SD_width + SD_gap)+SD_gap/2;


	TILE_hex_style1(height=moduleHeight, style="style1") {

		//Ajout matiere
		cube([0,0,0]);		//TODO: fix: forme vide

		//Enlevement matiere
		union() {
			rotate(150,[0,0,1])
			for (i=[1:MicroSD_cards])
				translate([5.8-USB_Holder_Length/2+(i-1)*(MicroSD_width+MicroSD_gap),-10,USB_Holder_Height-MicroSD_depth/2])
					#cube([MicroSD_width,MicroSD_length,MicroSD_depth], true);
			
			rotate(60,[0,0,1])
			for (i=[1:SD_cards])
			translate([-8+SD_gap+1.5-SD_Holder_Length/2+(i-1)*(SD_width+SD_gap),00,SD_Holder_Height-SD_depth/2])
				#cube([SD_width,SD_length,SD_depth+1], true);
		}
		
		
	}
}

MODULE_sd();