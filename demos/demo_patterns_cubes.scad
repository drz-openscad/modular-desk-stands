include <../LIB_STAND.scad>

espacement=10;

hauteurDecoupe=2;
hauteurPattern=40;

module object() {
	cylinder(100,7.5,7.5);
}

cube9 = [
	[-1,1],[0,1],[1,1],
	[-1,0],[0,0],[1,0],
	[-1,-1],[0,-1],[1,-1]
];

color("LightGreen")
	translate([0,50,50])
		%object();

color("LightGreen")
	translate([0,50,0])
		%PATTERN_cube(mapPattern=cube9, hauteurPattern=hauteurPattern);

color("DarkGray")
	translate([0,0,50])
		%PATTERN_cube_cut(mapPattern=cube9, hauteurPattern=hauteurPattern, hauteurDecoupe=hauteurDecoupe) {
			object();
		}

color("DarkGray")
	translate([0,0,0])
		%PATTERN_cube(mapPattern=cube9, hauteurPattern=hauteurPattern);

color("DarkBlue")
	translate([0,-50,0])
		UTIL_makeStand(hauteurPattern, hauteurDecoupe) {

			//BASE = pattern socle
			PATTERN_cube(mapPattern=cube9, espacementPattern=espacement, hauteurPattern=hauteurPattern);

			//CUT = objet decoupé avec pattern socle
			PATTERN_cube_cut(mapPattern=cube9, espacementPattern=espacement, hauteurDecoupe=hauteurDecoupe) {
				object();
			}
		}