include <../LIB_STAND.scad>


//Demo

TileSpacing=5;
ObjectHeight=100;
ModulesHeight=32;
CutHeight=2;


//Object to cut into the pattern(s)
module object() {
	$fn=100;
	cylinder(ObjectHeight,50,50);
}


//-----------------------------------------------------------------
//Demo Pattern "rnd7"
//-----------------------------------------------------------------

rnd7 = [
		[-1,1],			[0,1],

	[-1,0],		[0,0],		[1,0],

		[-1,-1], 		[0,-1]
];

translate([-200,0,0]) {

	//Object alone for demo purposes
	color("LightGreen")
		translate([0,200,50])
			%object();
	
	//Pattern alone for demo purposes
	color("LightGreen")
		translate([0,200,0])
			%PATTERN_hex(pattern=rnd7, height=ModulesHeight);

	//Object cut projection from pattern alone for demo purposes
	color("DarkGray")
		translate([0,0,50])
			%PATTERN_hex_cut(pattern=rnd7, height=CutHeight) {
				object();
			}
	//Pattern alone again for demo purposes
	color("DarkGray")
		translate([0,0,0])
			%PATTERN_hex(pattern=rnd7, height=ModulesHeight);


	//Result = Object cut projection into pattern + spacing
	color("DarkBlue")
		translate([0,-200,0])
			UTIL_makeStand(ModulesHeight, CutHeight) {

				//BASE = pattern socle
				PATTERN_hex(pattern=rnd7, spacing=TileSpacing, height=ModulesHeight);

				//CUT = objet decoupé avec pattern socle
				#PATTERN_hex_cut(pattern=rnd7, spacing=TileSpacing, height=CutHeight) {
					object();
				}
			}
}

//-----------------------------------------------------------------
//Demo Pattern "row3"
//-----------------------------------------------------------------

row3 = [
	[-1,0],[0,0],[1,0]
];

translate([200,0,0]) {

	//Object alone for demo purposes
	color("LightGreen")
		translate([0,200,50])
			%object();

	//Pattern alone for demo purposes
	color("LightGreen")
		translate([0,200,0])
		%PATTERN_hex(pattern=row3, height=ModulesHeight);

	//Object cut projection from pattern alone for demo purposes
	color("DarkGray")
		translate([0,0,50])

			%PATTERN_hex_cut(pattern=row3, height=CutHeight) {
				object();
			}
	
	//Pattern alone again for demo purposes
	color("DarkGray")
		translate([0,0,0])
			%PATTERN_hex(pattern=row3, height=ModulesHeight);

	//Result = Object cut projection into pattern + spacing
	color("DarkBlue")
		translate([0,-200,0])
			UTIL_makeStand(ModulesHeight, CutHeight) {

				//BASE = pattern socle
				PATTERN_hex(pattern=row3, spacing=TileSpacing, height=ModulesHeight);

				//CUT = objet decoupé avec pattern socle
				#PATTERN_hex_cut(pattern=row3, spacing=TileSpacing, height=CutHeight) {
					object();
				}
			}
}