include <../LIB_STAND.scad>
include <../LIB_LEGO.scad>

module TILE_hex_lego(height=HexTile_HeightUnit*1) {
	
	TILE_hex_default(height=height) {
		
		//Children 0: matiere ajoutée
		difference() {
			
			//positionnement du lego, avec picots "posés" a hauteur du module
			legoZ= (height-HexTile_BaseHeight)+.4;
			translate([0,0,legoZ])
				rotate(-30,[0,0,1])
					lego(8,8);
			
			//vire le bas de la piece lego (tout sauf les picots)
			cylinder(height, HexTile_Diameter, HexTile_Diameter);
				
			//vire les 4 picots "OCD" (non entiers)
			rotate(-30,[0,0,1])
				translate([-16,16,height])
					cube([8,8,8]);						
			rotate(-30,[0,0,1])
				translate([-16,-24,height])
					cube([8,8,8]);			
			rotate(-30,[0,0,1])
				translate([8,-24,height])
					cube([8,8,8]);									
			rotate(-30,[0,0,1])
				translate([8,16,height])
					cube([8,8,8]);
		}
	}
}

TILE_hex_lego(height=16);