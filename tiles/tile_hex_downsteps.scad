include <../LIB_STAND.scad>

module TILE_hex_downsteps(height=HexTile_HeightUnit*1) {
	
	stepHeight=0.4;
	steps=10;
	
	difference(){
		
		TILE_hex_default(height);
		
		startR=(HexTile_Diameter/2);
		for(x=[0:steps-1]) {
			curR=startR-(x*((startR)/(steps-1)));
			rotate(30,[0,0,1])
				translate([0,0,height-(stepHeight*(x-1))])
					cylinder(30, curR, curR, $fn=6);
		}
	}
}

TILE_hex_downsteps();