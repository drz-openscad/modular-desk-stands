include <../LIB_STAND.scad>

module TILE_hex_style1(height=HexTile_HeightUnit*1) {
	
	//inherit from TILE_hex_default
	TILE_hex_default(height=height-1) {
		//add 1mm chamfered hex
		translate([0,0,height-1])
			cylinder(1, HexTile_Diameter/2,HexTile_Diameter/2-2, $fn=6);
		children();
	}
}

TILE_hex_style1();