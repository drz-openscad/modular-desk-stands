#!/bin/bash

git diff --cached --name-status | while read st file; do
        # skip deleted files
        if [ $st == 'D' ]; then continue; fi

        # do a check only on the  scad files
        if [[ ${file: -5} == ".scad" ]]; then
                s=${file##*/}
                stl="stl/${s%.*}.stl"
                echo "Scad file changed: $file -> $stl";
                openscad -o $stl $file
        fi
done
