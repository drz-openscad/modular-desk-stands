include <PATTERNS_HEX.scad>
include <PATTERNS_CUBE.scad>

module UTIL_makeStand(baseHeight, offsetHeight) {
	translate([0,0,baseHeight]) {
		difference() {
			translate([0,0,-baseHeight])
				children(0);
			translate([0,0,-offsetHeight])
				children(1);
		}
	}
}
