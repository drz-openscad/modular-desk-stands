include <../LIB_STAND.scad>

module MODULE_test(moduleHeight=HexTile_HeightUnit*1, spacing=false) {

	patternLegs = [
			[-1,1],			[0,1],

		[-1,0],		/*[0,0]*/,		[1,0],

			[-1,-1], 		[0,-1]
	];

	PATTERN_hex(pattern=patternLegs, height=moduleHeight, spacing=spacing, style="downsteps");

	patternCenter = [
		[0,0]
	];

	PATTERN_hex(pattern=patternCenter, height=moduleHeight, spacing=spacing, style="style1");

}

MODULE_test();
