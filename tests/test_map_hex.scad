include <../LIB_STAND.scad>

module moduledemo(spacing=false) {
	patternModule1 = [
			[-1,1],			[0,1],

		[-1,0],		[0,0],		[1,0],

			[-1,-1], 		[0,-1]
	];
	PATTERN_hex(pattern=patternModule1, spacing=spacing);
}

map = [
	[0,0],
	[3,0],
	[1,-3]
];

MAP_hex(pattern=map, spacing=10) {
	moduledemo(spacing=spacing);
	%moduledemo(spacing=spacing);
	%moduledemo(spacing=spacing);
}