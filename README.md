# Modular, multiple shapes and patterns, multi-grid-modules, interlocking tiles desktop organizer system

(well, that's a long name !)

This toolset is a complete modular system to create an interlocking tile system for desktop organizing purposes.

## What can you do with this toolset ?

- Use a grid system, to compose any module of any pattern of shapes (provided ones, or yours)
- Split your module, so you can print individual tiles, based on the footprint of a larger object (making object stand across multiple tiles)
- Inherit from provided tiles, so you can design a new tile based on provided ones (exemple: TILE_hex + lego = TILE_hex_lego)
- Customize EVERYTHING !

## Samples !

Browse the 'stl/' files through the gitlab integrated viewer.

or

See 'demos/' folder content, and 'bureau.scad' code files :)

## How to use ?

TODO

## Workflow

1.Tile conception

TODO

2.Module conception



3.Module spliting for print

TODO

4.Map preview of all modules together

TODO

## TODO

(in order)

- [] finish rewriting french comments and variables in english
- [] finish the readme !
- [] thingiverse release
- [] more modules


## ♥ Pictures

### Early prototypes

![desk preview](pictures/20180220_154903.jpg)

### Desk preview

![desk preview](pictures/rEljPCt.png)

### Desk render

![desk render](pictures/CV8s8hq.png)

### Module design

![module design](pictures/5WuJeUf.png)

### Module print

![module print](pictures/1aVHkZv.png)