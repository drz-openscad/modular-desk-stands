$fn=75;

//Ato 22mm rond ET carré
ato_rond_diametre = 24.5;
ato_rond_profondeur = 6.4;
ato_pin_diametre = 7.2;
ato_pin_profondeur = 5.2;
ato_hauteur = ato_pin_profondeur+ato_rond_profondeur;
ato_carre_cote = ato_rond_diametre-2;

//Siguelei
sigelei_largeur = 48;
sigelei_longeur = 32;
sigelei_profondeur = 14+30;

//Bouteille 30ml
bouteille_rond_diametre = 35;
bouteille_rond_profondeur= 14;

//Drip tip
driptip_diametre = 9.2;
driptip_profondeur = 5.2;
	
module support_ato22mm() {
	
	cylinder(ato_pin_profondeur+.1, ato_pin_diametre/2,ato_pin_diametre/2);
	translate([0,0,ato_pin_profondeur])
		cylinder(ato_rond_profondeur+30, ato_rond_diametre/2,ato_rond_diametre/2);
	
		rotate(45,[0,0,1])
			translate([-ato_carre_cote/2,-ato_carre_cote/2,ato_pin_profondeur+4])
				cube([ato_carre_cote,ato_carre_cote, ato_rond_profondeur-3+30]);
		
		translate([-ato_carre_cote/2,-ato_carre_cote/2, ato_pin_profondeur+4])
			cube([ato_carre_cote, ato_carre_cote, ato_rond_profondeur-3+30]);
}

module support_sigelei() {
	translate([-sigelei_longeur/2, -sigelei_largeur/2, 0])
		cube([sigelei_longeur, sigelei_largeur, sigelei_profondeur]);
}

module support_bouteille30ml() {
	cylinder(bouteille_rond_profondeur+30, bouteille_rond_diametre/2,bouteille_rond_diametre/2);
}

module support_driptip() {
	cylinder(driptip_profondeur, driptip_diametre/2,driptip_diametre/2);
}

/*
module base() {
	
	$fn=12;
	radius=4;
	x=120;
	y=80;

	hull()
	{
		translate([(-x/2)+(radius/2), (-y/2)+(radius/2), 0])
		sphere(r=radius);

		translate([(x/2)-(radius/2), (-y/2)+(radius/2), 0])
		sphere(r=radius);
		
		translate([(-x/2)+(radius/2), (-y/2)+(radius/2), 16-(radius)])
		sphere(r=radius);

		translate([(x/2)-(radius/2), (-y/2)+(radius/2), 16-(radius)])
		sphere(r=radius);
		

		translate([(-x/2)+(radius/2), (y/2)-(radius/2), 32-(2*radius)])
		sphere(r=radius);

		translate([(x/2)-(radius/2), (y/2)-(radius/2), 32-(2*radius)])
		sphere(r=radius);
			
		translate([(-x/2)+(radius/2), (y/2)-(radius/2), 0])
		sphere(r=radius);

		translate([(x/2)-(radius/2), (y/2)-(radius/2), 0])
		sphere(r=radius);
	}	
}
*/

module slogan() {
	font1 = "Arial Unicode MS:style=Bold"; // here you can select other font type
	rotate(90,[1,0,0]) {
		linear_extrude(2) 
			text("MIKO",font=font1, h=11);	
	}		
}

module nuage() {
	//font1 = "Meslo LG M for Powerline:style=RegularForPowerline"; // here you can select other font type
	
	//font2 ="Arial Unicode MS:style=Bold";	
	//font2 ="Segoe UI Emoji:style=Regular";
	font2="Segoe UI Emoji:style=Regular";
	
	rotate(90,[1,0,0]) {
		linear_extrude(2) 
		//hull()
			text("☁",font=font2, size=12, h=11);
	}
}
/*
module all() {
	
	
	epaisseur_cube = 16;
	
	difference() {
		
		difference(){
			translate([60,40,0])
				base();
			translate([-10,-10,-10])
				cube([200,200,10]);
		}
		
		union() {
			
			zAtos = epaisseur_cube + 2 - ato_hauteur;
				
			//Ato 22 #1
			color([1,0,0])
				translate([20, 20, zAtos+.1])
					support_ato22mm();
			
			//Ato 22 #2
			color([1,0,0])
				translate([60, 20, zAtos+.1])
					support_ato22mm();
			
			
			zBouteilles = epaisseur_cube + 8 - bouteille_rond_profondeur;
			
			//Bouteille 1
			color([0,1,0])
				translate([20, 60, zBouteilles+.1])
					support_bouteille30ml();
			
			//Bouteille 2
			color([0,1,0])
				translate([60, 60, zBouteilles+.1])
					support_bouteille30ml();
			
			//Box
			zBox = epaisseur_cube - sigelei_profondeur;
			color([0,0,1])
				translate([100, 80-(sigelei_largeur/2)-3.2 , zBouteilles+.1])
					support_sigelei();
		
		
			//"Vide poche"
			zVidePoche = 22;
			//translate([80+((40-sigelei_longeur)/2),14, zVidePoche])
			//	rotate(90,[0,1,0])
			//		cylinder(sigelei_longeur,11,11);
						
			//Driptip1
			zDripTip1 =  epaisseur_cube + 3.2 - driptip_profondeur;
			color([0,1,1])
				translate([40, 8.8+4 , zDripTip1+.1])
					support_driptip();
			
			//Driptip2
			zDripTip2 =  epaisseur_cube + 6.2 - driptip_profondeur;
			color([0,1,1])
				translate([40, 31-4 , zDripTip2+.1])
					support_driptip();

			//Miko
			translate([45,0,2])
				slogan();
		
			//☁
			translate([5,0,1.2])
				nuage();
				
			//☁
			translate([93,0,1.2])
				nuage();

		}
	}

}
*/


module test_support_driptip() {
	
	largeur_cube = 40; 
	longeur_cube = 40;
	epaisseur_cube = 16;
	
	driptip_hauteur = epaisseur_cube - driptip_profondeur;
	
	difference() {
		translate([-largeur_cube/2, -longeur_cube/2, 0])
			cube([largeur_cube, longeur_cube, epaisseur_cube]);
		
		translate([0,0,driptip_hauteur+.1])
			support_driptip();
	}
}

module test_support_ato22mm() {
	
	largeur_cube = 40; 
	longeur_cube = 40;
	epaisseur_cube = 16;
	
	difference() {
		translate([-largeur_cube/2, -longeur_cube/2, 0])
			cube([largeur_cube, longeur_cube, epaisseur_cube]);
		translate([0,0,epaisseur_cube-ato_hauteur+.1])
			support_ato22mm();
	}
}

module test_support_bouteille30ml() {
	
	largeur_cube = 40; 
	longeur_cube = 40;
	epaisseur_cube = 16;
	
	bouteille_hauteur = epaisseur_cube - bouteille_rond_profondeur;
	
	difference() {
		translate([-largeur_cube/2, -longeur_cube/2, 0])
			cube([largeur_cube, longeur_cube, epaisseur_cube]);
		
		translate([0,0,bouteille_hauteur+.1])
			support_bouteille30ml();
	}
}

module test_support_sigelei() {
	
	largeur_cube = 40; 
	longeur_cube = 80;
	epaisseur_cube = 16;
	
	sigelei_hauteur = epaisseur_cube - sigelei_profondeur;
	
	difference() {
		translate([-largeur_cube/2, -longeur_cube/2, 0])
			cube([largeur_cube, longeur_cube, epaisseur_cube]);
		
		translate([-sigelei_longeur/2,-sigelei_largeur/2,sigelei_hauteur+.1])
			support_sigelei();
	}
}



//support_ato22mm();
//support_bouteille30ml();
//support_sigelei();
//test_support_ato22mm();
//test_support_driptip();
//test_support_bouteille30ml();
//test_support_sigelei();
