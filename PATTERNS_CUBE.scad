largeurCube=6;
longueurCube=6;

module TILE_cube(hauteurTile) {
	translate([-largeurCube/2,-longueurCube/2])
		cube([largeurCube, largeurCube, hauteurTile]);
}

module CUT_cube(hauteurDecoupe) {
	translate([-largeurCube/2,-longueurCube/2])
		cube([largeurCube, largeurCube, hauteurDecoupe]);
}

module MAP_cube(coords, espacementPattern) {
	for(i=[0:len(coords)-1]) {
		moduleCoords = coords[i];
		PLACE_cube(moduleCoords, espacementPattern=espacementPattern) {
			children(i);
		}
	}	
}

module PLACE_cube_cut(coords, espacementPattern) {
	objectX=coords[0]*espacementPattern;
	objectY=coords[1]*espacementPattern;
	translate([objectX,objectY]) {
		children();
	}
}

module PLACE_cube(coords, espacementPattern=0) {
	cubeX=coords[0]*(espacementPattern+largeurCube);
	cubeY=coords[1]*(espacementPattern+largeurCube);
	translate([cubeX,cubeY]) {
		children();
	}
}

module PATTERN_cube(mapPattern, espacementPattern=0, hauteurPattern) {
	for(i=[0:len(mapPattern)-1]) {
		PLACE_cube(coords=mapPattern[i], espacementPattern=espacementPattern) {
			TILE_cube(hauteurPattern);
		}
	}
}

module PATTERN_cube_cut(mapPattern, espacementPattern=0, hauteurDecoupe) {
	for(i=[0:len(mapPattern)-1]) {		
		render() {
			intersection() {
				PLACE_cube(coords=mapPattern[i], espacementPattern=espacementPattern) {
					CUT_cube(hauteurDecoupe);
				}
				
				PLACE_cube_cut(coords=mapPattern[i], espacementPattern=espacementPattern) {
					children();
				}
			}
		}
	}
}
